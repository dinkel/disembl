
# DisEMBL

A great challenge in the proteomics and structural genomics era is to predict
protein structure and function, including identification of those proteins
that are partially or wholly unstructured. Disordered regions in proteins
often contain short linear peptide motifs (e.g. SH3-ligands and targeting
signals) that are important for protein function. Linear peptide sites are
catalogued by [ELM](elm.eu.org).

DisEMBL is a computational tool for prediction of disordered/unstructured
regions within a protein sequence. As no clear definition of disorder exists,
we have developed parameters based on several alternative definitions, and
introduced a new one based on the concept of ''hot loops'', i.e. coils with
high temperature factors.

Avoiding potentially disordered segments in protein expression constructs can
increase expression, foldability and stability of the expressed protein.
DisEMBL is thus useful for target selection and the design of constructs as
needed for many biochemical studies, particularly structural biology and
structural genomics projects.

If you need further help please contact [Rune Linding](linding.science).

---

The DisEMBL pipeline package is released under the
[GPL](http://www.opensource.org/licenses/gpl-license.php) license and is
thereby OSI Certified Open Source Software.

The smoothing function sav_gol.c cannot be re-distributed but can be
downloaded from [TISEAN](http://www.mpipks-dresden.mpg.de/%7Etisean/).
